# PhoneBook
It's a Spring project for add contacts and search them 

## Getting Started
The project has only RESTful API and has no front-end.<br>
Build project and run it, and you can test it with Postman.<br>
There is a postman collection in Doc folder for testing.<br>

### Prerequisites

For run this project, you need these tools:
- JDK 11