package com.asredanesh.phonebook.service.impl;

import com.asredanesh.phonebook.response.GithubRepoResponse;
import com.asredanesh.phonebook.service.GithubService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

import java.time.Duration;
import java.util.*;

@Service
public class GithubServiceImpl implements GithubService {
    Logger logger = LoggerFactory.getLogger(GithubServiceImpl.class);

    private final RestTemplate restTemplate;
    private final String baseUrl = "https://api.github.com/users/";

    public GithubServiceImpl(RestTemplateBuilder restTemplateBuilder) {
        // set connection and read timeouts
        this.restTemplate = restTemplateBuilder
                .setConnectTimeout(Duration.ofSeconds(1000))
                .setReadTimeout(Duration.ofSeconds(1000))
                .build();
    }

    @Override
    public List<GithubRepoResponse> getRepositories(String username) {

        Integer page = 1;
        String url = baseUrl + username +"/repos?per_page=100&page=";

        List<GithubRepoResponse> repoList = new ArrayList<>();

        HttpHeaders header = new HttpHeaders();
        header.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        header.set("x-request-src", "desktop");
        HttpEntity<String> headerEntity = new HttpEntity<>("body", header);

        try {
            HttpStatus statusCode=null;
            do{
                ResponseEntity<GithubRepoResponse[]> response = this.restTemplate.exchange(url+page, HttpMethod.GET, headerEntity, GithubRepoResponse[].class);
                if (response.getStatusCode() == HttpStatus.OK) {
                    List<GithubRepoResponse> responseList = Arrays.asList(Objects.requireNonNull(response.getBody()));
                    if (responseList.size() != 0){
                        repoList.addAll(responseList);
                        page++;
                    }else {
                        return repoList;
                    }
                } else {
                    return null;
                }
            }while (true);

        } catch (HttpClientErrorException ex) {
            String responseStatusCode = ex.getStatusCode().toString();

            if (responseStatusCode.equals("404 NOT_FOUND")){
                String msg = "Github username not found.";
                logger.error(msg);
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, msg);
            } else {
                String responseBody = ex.getResponseBodyAsString();
                logger.error("{}:{}", responseStatusCode, responseBody, ex);
                throw new ResponseStatusException(HttpStatus.valueOf(responseStatusCode), responseBody);
            }
        }

    }
}
