package com.asredanesh.phonebook.service.impl;


import com.asredanesh.phonebook.dto.ContactDto;
import com.asredanesh.phonebook.dto.ContactSearchDto;
import com.asredanesh.phonebook.entity.ContactEntity;
import com.asredanesh.phonebook.entity.RepoEntity;
import com.asredanesh.phonebook.repository.ContactRepository;
import com.asredanesh.phonebook.response.GithubRepoResponse;
import com.asredanesh.phonebook.service.ContactService;
import com.asredanesh.phonebook.service.GithubService;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class ContactServiceImpl implements ContactService {
    Logger logger = LoggerFactory.getLogger(ContactServiceImpl.class);
    private ModelMapper modelMapper = new ModelMapper();

    @Autowired
    private ContactRepository contactRepository;

    @Autowired
    GithubService githubService;

    @Override
    public ContactDto add(ContactDto contactDto) {
        ContactEntity contactEntity = modelMapper.map(contactDto, ContactEntity.class);
        if (contactEntity.getName().isBlank()){

        }
        if (contactRepository.findByName(contactEntity.getName()) != null) {
            String msg = "Name is duplicate.";
            logger.error(msg);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, msg);
        }

        List<GithubRepoResponse> repoResponseList= new ArrayList<>();
        if (contactEntity.getGithub()!= null){
            repoResponseList = githubService.getRepositories(contactEntity.getGithub());
        }

        if(repoResponseList == null){
            String msg = "There is problem that connect to github api.";
            logger.error(msg);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, msg);
        }

        Set<RepoEntity> repositoriesList = new HashSet<>();

        for(GithubRepoResponse repo : repoResponseList){
            RepoEntity repoEntity = new RepoEntity();
            repoEntity.setName(repo.getName());
            repoEntity.setContactEntity(contactEntity);

            repositoriesList.add(repoEntity);
        }

        contactEntity.setRepositorySize(repoResponseList.size());
        contactEntity.setRepositoriesList(repositoriesList);

        ContactEntity userStored = contactRepository.save(contactEntity);
        return modelMapper.map(userStored, ContactDto.class);
    }

    @Override
    public List<ContactDto> search(ContactSearchDto contactDto) {
        List<ContactEntity> contactEntityList = contactRepository.findAll();

        if (!contactDto.getName().isBlank()){
            contactEntityList =
                    contactEntityList.stream()
                            .filter(contact -> contact.getName().equals(contactDto.getName()))
                    .collect(Collectors.toList());
        }

        if (!contactDto.getPhoneNumber().isBlank()){
            contactEntityList =
                    contactEntityList.stream()
                            .filter(contact -> contact.getPhoneNumber().equals(contactDto.getPhoneNumber()))
                            .collect(Collectors.toList());
        }

        if (!contactDto.getEmail().isBlank()){
            contactEntityList =
                    contactEntityList.stream()
                            .filter(contact -> contact.getEmail().equals(contactDto.getEmail()))
                            .collect(Collectors.toList());
        }

        if (!contactDto.getOrganization().isBlank()){
            contactEntityList =
                    contactEntityList.stream()
                            .filter(contact -> contact.getOrganization().equals(contactDto.getOrganization()))
                            .collect(Collectors.toList());
        }

        if (!contactDto.getGithub().isBlank()){
            contactEntityList =
                    contactEntityList.stream()
                            .filter(contact -> contact.getGithub().equals(contactDto.getGithub()))
                            .collect(Collectors.toList());
        }

        List<ContactDto> contactDtoList
                = modelMapper.map(contactEntityList, new TypeToken<List<ContactDto>>() {
        }.getType());

        return contactDtoList;
    }

}
