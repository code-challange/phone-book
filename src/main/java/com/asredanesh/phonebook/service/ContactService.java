package com.asredanesh.phonebook.service;

import com.asredanesh.phonebook.dto.ContactDto;
import com.asredanesh.phonebook.dto.ContactSearchDto;

import java.util.List;


public interface ContactService {
    ContactDto add(ContactDto contactDto);

    List<ContactDto> search(ContactSearchDto contactDto);
}
