package com.asredanesh.phonebook.service;

import com.asredanesh.phonebook.response.GithubRepoResponse;

import java.util.List;

public interface GithubService {
    List<GithubRepoResponse> getRepositories(String username);
}
