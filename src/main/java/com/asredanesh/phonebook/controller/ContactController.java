package com.asredanesh.phonebook.controller;

import com.asredanesh.phonebook.dto.ContactDto;
import com.asredanesh.phonebook.dto.ContactSearchDto;
import com.asredanesh.phonebook.request.ContactAddRequest;
import com.asredanesh.phonebook.request.ContactSearchRequest;
import com.asredanesh.phonebook.response.ContactAddResponse;
import com.asredanesh.phonebook.response.ContactSearchResponse;
import com.asredanesh.phonebook.service.ContactService;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/contacts")
public class ContactController {
    private ModelMapper modelMapper = new ModelMapper();

    @Autowired
    private ContactService contactService;

    @PutMapping()
    public ContactAddResponse add(@Valid @RequestBody ContactAddRequest contactReq) {
        ContactDto contactDto = modelMapper.map(contactReq, ContactDto.class);
        ContactDto contactStored = contactService.add(contactDto);

        return modelMapper.map(contactStored, ContactAddResponse.class);
    }

    @PostMapping("/search")
    public List<ContactSearchResponse> Contact(@RequestBody ContactSearchRequest contactReq) {
        ContactSearchDto contactSearchDto = modelMapper.map(contactReq, ContactSearchDto.class);
        List<ContactDto> contactStored = contactService.search(contactSearchDto);
        List<ContactSearchResponse> contactSearchResponseLis
                = modelMapper.map(contactStored, new TypeToken<List<ContactSearchResponse>>() {}.getType());

        return contactSearchResponseLis;
      }

}
