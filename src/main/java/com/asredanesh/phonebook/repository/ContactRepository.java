package com.asredanesh.phonebook.repository;

import com.asredanesh.phonebook.entity.ContactEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContactRepository extends JpaRepository<ContactEntity,Long> {
    ContactEntity findByName(String name);
}
