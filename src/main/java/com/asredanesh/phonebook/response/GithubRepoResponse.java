package com.asredanesh.phonebook.response;

public class GithubRepoResponse {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
