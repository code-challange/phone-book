package com.asredanesh.phonebook.dto;

import java.util.HashSet;
import java.util.Set;

public class ContactDto {

    private Long id;
    private String name;
    private String phoneNumber;
    private String email;
    private String organization;
    private String github;
    private Set<RepoDto> repositoriesList = new HashSet<>();
    private Integer RepositorySize;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getGithub() {
        return github;
    }

    public void setGithub(String github) {
        this.github = github;
    }

    public Set<RepoDto> getRepositoriesList() {
        return repositoriesList;
    }

    public void setRepositoriesList(Set<RepoDto> repositoriesList) {
        this.repositoriesList = repositoriesList;
    }

    public Integer getRepositorySize() {
        return RepositorySize;
    }

    public void setRepositorySize(Integer repositorySize) {
        RepositorySize = repositorySize;
    }
}


